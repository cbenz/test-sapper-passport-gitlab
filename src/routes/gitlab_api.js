import Gitlab from "gitlab"

export async function get(req, res) {
  const { user } = req
  if (!user) {
    return res.status(401).send({ err: "Unauthorized" })
  }

  const api = new Gitlab({
    url: "https://framagit.org",
    oauthToken: user.accessToken
  })

  const { action } = req.query

  switch (action) {
    case "create_project":
      const { name } = req.query
      let project
      try {
        project = await api.Projects.create({ name })
        // console.log(project)
      } catch (err) {
        // console.log(err)
        if (err.statusCode === 401) {
          return res.redirect(`/login`) // TODO returnTo
        }
        res.setHeader("Content-Type", "application/json")
        return res.status(err.statusCode).end(JSON.stringify(err))
      }
      res.setHeader("Content-Type", "application/json")
      return res.end(JSON.stringify({ project }))

    case "list_projects":
      let projects
      try {
        projects = await api.Users.projects(user.id)
        // console.log(projects)
      } catch (err) {
        // TODO Factor this error handling code
        // console.log(err)
        if (err.statusCode === 401) {
          return res.redirect(`/login`) // TODO returnTo
        }
        res.setHeader("Content-Type", "application/json")
        return res.status(err.statusCode).end(JSON.stringify(err))
      }
      res.setHeader("Content-Type", "application/json")
      return res.end(JSON.stringify({ projects }))
  }

  res.sendStatus(404)
}
