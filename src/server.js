import sirv from "sirv"
import passport from "passport"
import { Strategy as GitLabStrategy } from "passport-gitlab2"
import express from "express"
import session from "express-session"
import SessionFileStore from "session-file-store"
import querystring from "querystring"
import compression from "compression"
import * as sapper from "../__sapper__/server.js"
import { Store } from "svelte/store.js"
import url from "url"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const FileStore = SessionFileStore(session)

passport.use(
  new GitLabStrategy(
    {
      clientID:
        "fd199b32367586d4d88132f1f05f5352a598c649c82866b8ed75a23611fc1d5f",
      clientSecret:
        "100975ec210bc28522e3c11dd76c0d67e04787b5a505a0e3e8f523d344115da9",
      callbackURL: "http://localhost:7000/auth/gitlab/callback",
      baseURL: "https://framagit.org"
    },
    function(accessToken, refreshToken, profile, cb) {
      return cb(null, { ...profile, accessToken })
    }
  )
)

passport.serializeUser(function(user, cb) {
  cb(null, user)
})

passport.deserializeUser(function(obj, cb) {
  cb(null, obj)
})

// The middleware to set up the parameters for the authenticate middleware.
// From https://gist.github.com/tchap/5643644
function checkReturnTo(req, res, next) {
  var returnTo = req.query["returnTo"]
  if (returnTo) {
    // Maybe unnecessary, but just to be sure.
    req.session = req.session || {}

    // Set returnTo to the path you want to be redirect to after the authentication succeeds.
    req.session.returnTo = querystring.unescape(returnTo)
  }
  next()
}

express()
  .use(
    session({
      saveUninitialized: false,
      secret: "TODO",
      store: new FileStore()
    })
  )
  .use(passport.initialize())
  .use(passport.session())
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      ignore: [/^\/auth.+/, "/login", "/logout"], // otherwise ".get" calls below don't get called
      store: request => {
        return new Store({
          user: request.user
        })
      }
    })
  )

  // use routes/login.html to let the user choose an identity provider among many
  .get("/login", (req, res) => {
    let path = "/auth/gitlab"
    const qs = url.parse(req.url).query
    if (qs) {
      path += `?${qs}`
    }
    res.redirect(path)
  })
  .get(
    "/auth/gitlab",
    checkReturnTo,
    passport.authenticate("gitlab", {
      scope: ["api"]
    })
  )
  .get(
    "/auth/gitlab/callback",
    passport.authenticate("gitlab", {
      successReturnToOrRedirect: "/",
      failureRedirect: "/error/account-not-found"
    })
  )
  .get("/logout", function(req, res) {
    req.logout()
    res.redirect("/")
  })

  .listen(PORT, err => {
    if (err) console.log("error", err)
  })
